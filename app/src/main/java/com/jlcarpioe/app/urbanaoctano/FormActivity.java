package com.jlcarpioe.app.urbanaoctano;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.jlcarpioe.app.urbanaoctano.enums.API_ROUTE;
import com.jlcarpioe.app.urbanaoctano.enums.PARAM_TYPE;
import com.jlcarpioe.app.urbanaoctano.interfaces.ApiErrorListener;
import com.jlcarpioe.app.urbanaoctano.models.ApiError;
import com.jlcarpioe.app.urbanaoctano.models.ApiParam;
import com.jlcarpioe.app.urbanaoctano.models.Center;
import com.jlcarpioe.app.urbanaoctano.net.RequestsManager;
import com.jlcarpioe.app.urbanaoctano.utils.Util;
import com.jlcarpioe.app.urbanaoctano.utils.Validator;
import com.jlcarpioe.app.urbanaoctano.widgets.ProgressLoader;

import java.util.ArrayList;


/**
 * FormActivity
 * Handler form to create/modify a center.
 *
 * @author José Luis Carpio E. <jlcarpioe@gmail.com>.
 *
 */
public class FormActivity extends AppCompatActivity {

	private static final String TAG = FormActivity.class.getSimpleName();


	// UI references.
	private View mContainer;
	private TextInputEditText mNameInput;
	private TextInputEditText mAddressInput;
	private TextInputEditText mLatitudeInput;
	private TextInputEditText mLongitudeInput;
	private Button mButton;

	private ProgressLoader mProgressLoader;
	private Center centerToUpdate;
	private Boolean isUpdate;


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_form);
		findViewElements();
		isUpdate = false;

		// Instance progress loader
		mProgressLoader = ProgressLoader.setup(this);

		// check if it is going to update
		String centerJsonString = getIntent().getStringExtra("center_data");
		if (centerJsonString != null) {
			isUpdate = true;
			Gson gson = new Gson();
			centerToUpdate = gson.fromJson(centerJsonString, Center.class);

			// Assign data
			mNameInput.setText(centerToUpdate.getName());
			mAddressInput.setText(centerToUpdate.getAddress());
			mLatitudeInput.setText(centerToUpdate.getLatitude());
			mLongitudeInput.setText(centerToUpdate.getLongitude());
		}

		// Action button
		mButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Util.hideKeyboard(FormActivity.this);

				// check required fields
				if ( Validator.isEmpty(mNameInput) || Validator.isEmpty(mAddressInput) ||
						Validator.isEmpty(mLatitudeInput) || Validator.isEmpty(mLongitudeInput)) {

					// Snackbar info
					Snackbar snackbar = Snackbar.make(mContainer, R.string.all_field_required, Snackbar.LENGTH_LONG);
					View snackbarView = snackbar.getView();
					snackbarView.setBackgroundColor(ContextCompat.getColor(FormActivity.this, R.color.red));
					snackbar.show();

				}
				else if (isUpdate) {
					updateData();
				}
				else {
					saveData();
				}
			}
		});
	}


	/**
	 * Find elements on layout
	 */
	private void findViewElements() {
		mContainer = findViewById(R.id.form_container);
		mNameInput = findViewById(R.id.center_name);
		mAddressInput = findViewById(R.id.center_address);
		mLatitudeInput = findViewById(R.id.center_latitude);
		mLongitudeInput = findViewById(R.id.center_longitude);
		mButton = findViewById(R.id.form_button);
	}


	/**
	 * Save data of center
	 *
	 */
	private void saveData() {

		// Show loader while request is execute
		mProgressLoader.show();

		// Create object to send on request
		Center center = new Center();
		center.setId(String.valueOf(System.currentTimeMillis()));
		center.setName(mNameInput.getText().toString());
		center.setAddress(mAddressInput.getText().toString());
		center.setLatitude(mLatitudeInput.getText().toString());
		center.setLongitude(mLongitudeInput.getText().toString());

		// Make request
		RequestsManager.request(
				this,
				new ArrayList<ApiParam>(),
				center.getJsonStringFields(),
				API_ROUTE.CREATE_CENTER,
				Center.class,
				ApiError.class,
				new Response.Listener<Center>() {
					@Override
					public void onResponse(Center response) {

						Log.d(TAG, response.getJsonStringFields());

						mProgressLoader.dismiss();

						// Alert message
						new AlertDialog.Builder(FormActivity.this)
								.setCancelable(false)
								.setMessage(R.string.success_create_center)
								.setPositiveButton(R.string.go_ahead, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.dismiss();

										// cancel animation on change activities
										overridePendingTransition(0, 0);

										setResult(RESULT_OK);
										finish();
									}
								})
								.create()
								.show();
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {

						mProgressLoader.dismiss();

						// Alert message
						new AlertDialog.Builder(FormActivity.this)
								.setMessage(R.string.request_error)
								.setPositiveButton(R.string.go_ahead, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.dismiss();
									}
								})
								.create()
								.show();

					}
				},
				new ApiErrorListener<ApiError>() {
					@Override
					public void onApiError(ApiError apiError) {

						mProgressLoader.dismiss();

						// Snackbar info
						Snackbar snackbar = Snackbar.make(mContainer, R.string.error_create_center, Snackbar.LENGTH_LONG);
						View snackbarView = snackbar.getView();
						snackbarView.setBackgroundColor(ContextCompat.getColor(FormActivity.this, R.color.red));
						snackbar.show();

					}
				}
		);
	}


	/**
	 * Update data of center
	 *
	 */
	private void updateData() {

		// Show loader while request is execute
		mProgressLoader.show();

		// Create object to send on request
		Center center = new Center();
		center.setId(centerToUpdate.getId());
		center.setName(mNameInput.getText().toString());
		center.setAddress(mAddressInput.getText().toString());
		center.setLatitude(mLatitudeInput.getText().toString());
		center.setLongitude(mLongitudeInput.getText().toString());

		// Param URL request
		ArrayList<ApiParam> params = new ArrayList<>();
		params.add(new ApiParam("id", PARAM_TYPE.URL_PARAM, centerToUpdate.getId()));

		// Make request
		RequestsManager.request(
				this,
				params,
				center.getJsonString(),
				API_ROUTE.UPDATE_CENTER,
				Center.class,
				ApiError.class,
				new Response.Listener<Center>() {
					@Override
					public void onResponse(Center response) {

						Log.d(TAG, response.getJsonStringFields());

						mProgressLoader.dismiss();

						// Alert message
						new AlertDialog.Builder(FormActivity.this)
								.setCancelable(false)
								.setMessage(R.string.success_update_center)
								.setPositiveButton(R.string.go_ahead, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.dismiss();

										// cancel animation on change activities
										overridePendingTransition(0, 0);

										setResult(RESULT_OK);
										finish();
									}
								})
								.create()
								.show();
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {

						mProgressLoader.dismiss();

						// Alert message
						new AlertDialog.Builder(FormActivity.this)
								.setMessage(R.string.request_error)
								.setPositiveButton(R.string.go_ahead, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.dismiss();
									}
								})
								.create()
								.show();

					}
				},
				new ApiErrorListener<ApiError>() {
					@Override
					public void onApiError(ApiError apiError) {

						mProgressLoader.dismiss();

						// Snackbar info
						Snackbar snackbar = Snackbar.make(mContainer, R.string.error_update_center, Snackbar.LENGTH_LONG);
						View snackbarView = snackbar.getView();
						snackbarView.setBackgroundColor(ContextCompat.getColor(FormActivity.this, R.color.red));
						snackbar.show();

					}
				}
		);
	}

}
