package com.jlcarpioe.app.urbanaoctano;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.jlcarpioe.app.urbanaoctano.enums.API_ROUTE;
import com.jlcarpioe.app.urbanaoctano.enums.PARAM_TYPE;
import com.jlcarpioe.app.urbanaoctano.interfaces.ApiErrorListener;
import com.jlcarpioe.app.urbanaoctano.models.ApiError;
import com.jlcarpioe.app.urbanaoctano.models.ApiParam;
import com.jlcarpioe.app.urbanaoctano.models.Center;
import com.jlcarpioe.app.urbanaoctano.net.RequestsManager;
import com.jlcarpioe.app.urbanaoctano.widgets.ProgressLoader;

import java.util.ArrayList;


/**
 * DetailActivity
 * Handle detail of center.
 *
 * @author José Luis Carpio E. <jlcarpioe@gmail.com>.
 *
 */
public class DetailActivity extends AppCompatActivity {

	private static final String TAG = DetailActivity.class.getSimpleName();


	// UI references.
	private View mContainer;
	private TextInputEditText mNameInput;
	private TextInputEditText mAddressInput;
	private TextInputEditText mLatitudeInput;
	private TextInputEditText mLongitudeInput;

	private ProgressLoader mProgressLoader;
	private Center center;


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_form);
		findViewElements();

		// Instance progress loader
		mProgressLoader = ProgressLoader.setup(this);

		// Get center info
		Bundle bundle = getIntent().getExtras();
		Gson gson = new Gson();
		center = gson.fromJson(bundle.getString("center_data", null), Center.class);

		// Assign data
		mNameInput.setText(center.getName());
		mAddressInput.setText(center.getAddress());
		mLatitudeInput.setText(center.getLatitude());
		mLongitudeInput.setText(center.getLongitude());

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_center_detail, menu);
		return true;
		//return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.action_delete) {
			new AlertDialog.Builder(this)
					.setMessage(R.string.delete_confirm)
					.setNegativeButton(R.string.cancel, null)
					.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							delete();
						}
					})
					.create()
					.show();
		}
		else if (item.getItemId() == R.id.action_edit) {

			// cancel animation on change activities
			overridePendingTransition(0, 0);

			// change activity
			Intent intent = new Intent(this, FormActivity.class);
			intent.putExtra("center_data", center.getJsonStringFields());
			startActivity(intent);

			// end this activity
			setResult(RESULT_CANCELED);
			finish();

		}
		return super.onOptionsItemSelected(item);
	}



	/**
	 * Find elements on layout
	 */
	private void findViewElements() {
		mContainer = findViewById(R.id.form_container);
		mNameInput = findViewById(R.id.center_name);
		mAddressInput = findViewById(R.id.center_address);
		mLatitudeInput = findViewById(R.id.center_latitude);
		mLongitudeInput = findViewById(R.id.center_longitude);

		// view's properties
		findViewById(R.id.form_button).setVisibility(View.GONE);
		mNameInput.setEnabled(false);
		mAddressInput.setEnabled(false);
		mLatitudeInput.setEnabled(false);
		mLongitudeInput.setEnabled(false);
	}

	/**
	 * Delete a center calling API
	 */
	private void delete() {

		// Show loader while request is execute
		mProgressLoader.show();

		// params request
		ArrayList<ApiParam> params = new ArrayList<>();
		params.add(new ApiParam("id", PARAM_TYPE.URL_PARAM, center.getId()));

		// make request
		RequestsManager.request(
				this,
				params,
				API_ROUTE.DELETE_CENTER,
				Integer.class,
				ApiError.class,
				new Response.Listener<Integer>() {
					@Override
					public void onResponse(Integer response) {
						Log.d(TAG, "Resp :: " + response);

						mProgressLoader.dismiss();

						// Alert message
						new AlertDialog.Builder(DetailActivity.this)
								.setCancelable(false)
								.setMessage(R.string.success_delete_center)
								.setPositiveButton(R.string.go_ahead, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.dismiss();
										setResult(RESULT_OK);
										finish();
									}
								})
								.create()
								.show();
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.e(TAG, "VolleyError :: " + error.getMessage());

						mProgressLoader.dismiss();

						// Snackbar info
						Snackbar snackbar = Snackbar.make(mContainer, R.string.error_delete_center, Snackbar.LENGTH_LONG);
						View snackbarView = snackbar.getView();
						snackbarView.setBackgroundColor(ContextCompat.getColor(DetailActivity.this, R.color.red));
						snackbar.show();
					}
				},
				new ApiErrorListener<ApiError>() {
					@Override
					public void onApiError(ApiError apiError) {

						mProgressLoader.dismiss();

						// Snackbar info
						Snackbar snackbar = Snackbar.make(mContainer, R.string.error_delete_center, Snackbar.LENGTH_LONG);
						View snackbarView = snackbar.getView();
						snackbarView.setBackgroundColor(ContextCompat.getColor(DetailActivity.this, R.color.red));
						snackbar.show();

					}
				}
		);

	}

}
