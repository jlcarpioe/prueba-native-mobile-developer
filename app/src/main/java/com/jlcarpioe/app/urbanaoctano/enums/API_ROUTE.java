package com.jlcarpioe.app.urbanaoctano.enums;

import com.android.volley.Request;


/**
 * API_ROUTE
 * {@link Enum} List of operations for api services.
 *
 * This code is under the MIT License (MIT). See LICENSE file.
 *
 * @author José Luis Carpio E. <jlcarpioe@gmail.com>
 *
 */
public enum API_ROUTE {

    LOGIN           (Request.Method.POST, "auth"),
    LIST_CENTERS    (Request.Method.GET, "centros"),
    DETAIL_CENTER   (Request.Method.GET, "centros/:id"),
    CREATE_CENTER   (Request.Method.POST, "centros"),
    UPDATE_CENTER   (Request.Method.PUT, "centros/:id"),
	DELETE_CENTER   (Request.Method.DELETE, "centros/:id");


    private String path;
    private int method;

    API_ROUTE(int method, String path) {
        this.method = method;
        this.path = path;
    }

    public int getMethod() {
	    return method;
    }

    public String getPath() {
        return path;
    }

}
