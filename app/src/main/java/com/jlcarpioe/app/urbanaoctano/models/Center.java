package com.jlcarpioe.app.urbanaoctano.models;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Modifier;


public class Center {

	@Expose
	private String id;

	@Expose
	private String name;

	@Expose
	private String address;

	@Expose
	private String latitude;

	@Expose
	private String longitude;

	@Expose(serialize = false)
	@SerializedName("created_at")
	private String createdAt;

	@Expose(serialize = false)
	@SerializedName("updated_at")
	private String updatedAt;




	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public String getLatitude() {
		return latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}


	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}


	/**
	 * Get json string of class
	 *
	 * @return String json
	 */
	public String getJsonString() {
		Gson gson = new GsonBuilder()
				.excludeFieldsWithoutExposeAnnotation()
				.excludeFieldsWithModifiers(Modifier.STATIC)
				.create();

		return gson.toJson(this);
	}

	/**
	 * Get json string of class with all values
	 *
	 * @return String json
	 */
	public String getJsonStringFields() {
		Gson gson = new GsonBuilder()
				.excludeFieldsWithModifiers(Modifier.STATIC)
				.create();

		return gson.toJson(this);
	}

}
