package com.jlcarpioe.app.urbanaoctano.interfaces;

/**
 * ApiErrorListener
 * Actions listener for error on API.
 *
 * @author José Luis Carpio E. <jlcarpioe@gmail.com>
 *
 */
public interface ApiErrorListener<T> {

    void onApiError(T apiError);

}
