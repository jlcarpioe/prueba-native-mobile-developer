package com.jlcarpioe.app.urbanaoctano.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jlcarpioe.app.urbanaoctano.R;
import com.jlcarpioe.app.urbanaoctano.interfaces.OnListCenterListener;
import com.jlcarpioe.app.urbanaoctano.models.Center;

import java.util.ArrayList;
import java.util.List;


/**
 * CentersAdapter
 * Adapter to manage centers information list.
 *
 * @author José Luis Carpio E. <jlcarpioe@gmail.com>.
 *
 */
public class CentersAdapter extends RecyclerView.Adapter<CentersAdapter.CustomViewHolder> {

	private List<Center> items;
	private List<Center> mDataset;
	private OnListCenterListener mListener;


	public CentersAdapter(List<Center> items, OnListCenterListener listener) {
		this.items = items;
		this.mListener = listener;

		// Set original data
		mDataset = new ArrayList<>();
		mDataset.addAll(items);
	}

	@Override
	public int getItemCount() {
		return items.size();
	}

	@Override
	public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_center, parent, false);
		return new CustomViewHolder(v);
	}

	@Override
	public void onBindViewHolder(final CustomViewHolder holder, int position) {
		final Center e = items.get(position);

		// set info
		holder.name.setText(e.getName());
		holder.address.setText(e.getAddress());

		// set click listener
		holder.itemView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if ( mListener != null ) {
					mListener.onCenterSelected(e);
				}
			}
		});
	}



	/**
	 * ViewHolder class for layout.
	 *
	 */
	static class CustomViewHolder extends RecyclerView.ViewHolder {

		View itemView;
		TextView name;
		TextView address;

		CustomViewHolder(View itemView) {
			super(itemView);

			this.itemView = itemView;
			this.name = itemView.findViewById(R.id.center_name);
			this.address = itemView.findViewById(R.id.center_address);
		}
	}
}
