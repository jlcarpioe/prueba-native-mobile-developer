package com.jlcarpioe.app.urbanaoctano;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.jlcarpioe.app.urbanaoctano.adapters.CentersAdapter;
import com.jlcarpioe.app.urbanaoctano.enums.API_ROUTE;
import com.jlcarpioe.app.urbanaoctano.interfaces.ApiErrorListener;
import com.jlcarpioe.app.urbanaoctano.interfaces.OnListCenterListener;
import com.jlcarpioe.app.urbanaoctano.models.ApiError;
import com.jlcarpioe.app.urbanaoctano.models.ApiParam;
import com.jlcarpioe.app.urbanaoctano.models.Center;
import com.jlcarpioe.app.urbanaoctano.models.ResponseListCenter;
import com.jlcarpioe.app.urbanaoctano.net.RequestsManager;
import com.jlcarpioe.app.urbanaoctano.widgets.ProgressLoader;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements OnListCenterListener {

	private static final String TAG = MainActivity.class.getSimpleName();

	public static final int REQUEST_CODE_SAVE = 3498;
	public static final int REQUEST_CODE_VIEW = 7532;


	private CentersAdapter mAdapter;
	private RecyclerView mRecyclerView;

	private ProgressLoader mProgressLoader;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// find views
		Toolbar toolbar = findViewById(R.id.toolbar);
		mRecyclerView = findViewById(R.id.recycler_centers);

		// Instance progress loader
		mProgressLoader = ProgressLoader.setup(this);

		// Set info for recyclerView
		LinearLayoutManager layoutManager = new LinearLayoutManager(this);
		mRecyclerView.setHasFixedSize(true);
		mRecyclerView.setLayoutManager(layoutManager);
		mRecyclerView.setAdapter(mAdapter);

		setSupportActionBar(toolbar);

		// Floating button to add center
		FloatingActionButton fab = findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(MainActivity.this, FormActivity.class);
				startActivityForResult(intent, REQUEST_CODE_SAVE);
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();

		// call API
		findCenters();
	}


	/**
	 * Find centers and assign to the view
	 *
	 */
	private void findCenters() {

		// Show loader while request is execute
		mProgressLoader.show();

		RequestsManager.request(
				this,
				new ArrayList<ApiParam>(),
				API_ROUTE.LIST_CENTERS,
				ResponseListCenter.class,
				ApiError.class,
				new Response.Listener<ResponseListCenter>() {
					@Override
					public void onResponse(ResponseListCenter response) {

						mProgressLoader.dismiss();

						// check list of centers
						if (response.getCenters().isEmpty()) {
							//TODO Mostrar mensaje de que no existen centros o fondo de que no hay registros
						}
						else {

							//TODO Se puede mejorar pasando el resultado del api como paginación y no obtenerlos todos de una vez
							mAdapter = new CentersAdapter(response.getCenters(), MainActivity.this);
							mRecyclerView.setAdapter(mAdapter);
						}

					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {

						mProgressLoader.dismiss();

					}
				},
				new ApiErrorListener<ApiError>() {
					@Override
					public void onApiError(ApiError apiError) {

						mProgressLoader.dismiss();

					}
				}
		);

	}

	@Override
	public void onCenterSelected(Center center) {

		// transform object to string
		String centerJson = center.getJsonStringFields();

		// change activity
		Intent intent = new Intent(this, DetailActivity.class);
		intent.putExtra("center_data", centerJson);
		startActivityForResult(intent, REQUEST_CODE_VIEW);

	}

}
