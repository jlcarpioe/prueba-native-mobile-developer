package com.jlcarpioe.app.urbanaoctano.net;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.jlcarpioe.app.urbanaoctano.interfaces.ApiErrorListener;
import com.jlcarpioe.app.urbanaoctano.utils.SharedPreference;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Modifier;
import java.util.Map;


/**
 * GsonRequestAPI.
 * Actions of {@link JsonRequest} to communicate with API using JSON.
 *
 * This code is under the MIT License (MIT). See LICENSE file.
 *
 * @author José Luis Carpio E. <jlcarpioe@gmail.com>.
 *
 */
public class GsonRequestAPI<T, S> extends JsonRequest<T> {

    private static final String TAG = GsonRequestAPI.class.getSimpleName();
    private final Gson gson;
    private final Class<T> sucessClass;
    private final Class<S> errorClass;
    private final Response.Listener<T> responseListener;
    private final ApiErrorListener<S> apiErrorListener;
    private final Map<String, String> headers;


	/**
	 * Assign data to request.
	 *
	 * @param context {@link Context}
	 * @param method Request method (GET, POST, UPDATE, DELETE)
	 * @param url URL of webservice operation.
	 * @param successClass Class where will save data when it is success.
	 * @param errorClass Class where will save data of error.
	 * @param headers Headers of request
	 * @param jsonBody Body of request
	 * @param responseListener Listener for success response.
	 * @param errorListener Listener for error response.
	 * @param apiErrorListener Listener for custom API error response.
	 *
	 */
    public GsonRequestAPI(Context context, int method, String url, Class<T> successClass,
                          Class<S> errorClass, Map<String, String> headers,
                          String jsonBody, Response.Listener<T> responseListener,
                          Response.ErrorListener errorListener,
                          ApiErrorListener<S> apiErrorListener) {

        super(method, url, jsonBody, responseListener, errorListener);

        this.gson = new GsonBuilder()
		        .excludeFieldsWithoutExposeAnnotation()
		        .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
		        .serializeNulls()
		        .create();

        this.sucessClass = successClass;
        this.responseListener = responseListener;
        this.headers = headers;
        this.errorClass = errorClass;
        this.apiErrorListener = apiErrorListener;

        // search and check if exists header token to authorize API request
	    String authToken = SharedPreference.getString(context, "token_header");
        if ( authToken != null ) {
            headers.put("Authorization", "Bearer " + authToken);
        }

	    // Log.d(TAG, "URL -> " + url);
//	    Log.d(TAG, "BODY -> " + jsonBody);
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return this.headers != null ? this.headers : super.getHeaders();
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {

            if (this.sucessClass != null) {

                // Log.d(TAG, "SUCCESS :: \n" + new String(response.data, HttpHeaderParser.parseCharset(response.headers)) );

                return Response.success(
                        this.gson.fromJson(
                        		new String(
                        				response.data,
				                        HttpHeaderParser.parseCharset(response.headers)
		                        ),
		                        this.sucessClass
                        ),
                        HttpHeaderParser.parseCacheHeaders(response));
            }

            throw new Exception("no deserializable class provided");

        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        } catch (Exception e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        }
    }


    @Override
    protected void deliverResponse(T response) {
        this.responseListener.onResponse(response);
    }


    @Override
    public void deliverError(VolleyError error) {
        try {
            if (this.errorClass != null && apiErrorListener != null ) {

//	            Log.d(TAG, "ERROR :: " + new String(error.networkResponse.data, HttpHeaderParser.parseCharset(error.networkResponse.headers)) );

                apiErrorListener.onApiError(
                		this.gson.fromJson(
                				new String(error.networkResponse.data, HttpHeaderParser.parseCharset(error.networkResponse.headers)),
				                this.errorClass
		                )
                );
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception - deliverError :: " + e.getMessage());
            super.deliverError(error);
        }
    }
}
