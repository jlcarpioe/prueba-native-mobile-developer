package com.jlcarpioe.app.urbanaoctano.utils;

import android.content.Context;
import android.content.SharedPreferences;


/**
 * SharedPreference
 * Manage SharedPreferences application to save and get values.
 *
 * This code is under the MIT License (MIT). See LICENSE file.
 *
 * @author José Luis Carpio E. <jlcarpioe@gmail.com>.
 *
 */
public class SharedPreference {

	private static final String FILE_SHARED_PREFERENCES = "pref_app";


	/**
	 * SharedPreferences of application
	 *
	 * @param context Context application
	 * @return SharedPreferences Object
	 */
	private static SharedPreferences getSharedPreferences(Context context) {
		return context.getSharedPreferences(FILE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
	}

	/**
	 * Save data on SharedPreferences
	 *
	 * @param context Context application
	 * @param key The name of the preference.
	 * @param value The value for the preference.
	 */
	public static void save(Context context, String key, Object value){
		SharedPreferences.Editor editor = getSharedPreferences(context).edit();

		if ( value instanceof String) {
			editor.putString(key, (String) value);
		}
		else if ( value instanceof Integer) {
			editor.putInt(key, (Integer) value);
		}
		else if ( value instanceof Long) {
			editor.putLong(key, (Long) value);
		}
		else if ( value instanceof Boolean) {
			editor.putBoolean(key, (Boolean) value);
		}

		editor.apply();
	}

	/**
	 * Retrieve a boolean value from the preferences.
	 *
	 * @param context Context application
	 * @param key The name of the preference to retrieve.
	 * @return Returns the preference value if it exists or false.
	 */
	public static boolean getBoolean(Context context, String key) {
		return getSharedPreferences(context).getBoolean(key, false);
	}

	/**
	 * Retrieve a string value from the preferences.
	 *
	 * @param context Context application
	 * @param key The name of the preference to retrieve.
	 * @return Returns the preference value if it exists or null.
	 */
	public static String getString(Context context, String key) {
		return getSharedPreferences(context).getString(key, null);
	}

	/**
	 * Retrieve a integer value from the preferences.
	 *
	 * @param context Context application
	 * @param key The name of the preference to retrieve.
	 * @return Returns the preference value if it exists or -1.
	 */
	public static Integer getInt(Context context, String key) {
		return getSharedPreferences(context).getInt(key, -1);
	}

	/**
	 * Retrieve a long value from the preferences.
	 *
	 * @param context Context application
	 * @param key The name of the preference to retrieve.
	 * @return Returns the preference value if it exists or -1.
	 */
	public static Long getLong(Context context, String key) {
		return getSharedPreferences(context).getLong(key, -1);
	}

	/**
	 * Remove all values from the preferences
	 *
	 * @param context Context application
	 * @return Boolean to indicate success or fail operation
	 */
	public static boolean clearAll(Context context) {
		return getSharedPreferences(context).edit().clear().commit();
	}

	/**
	 * Remove specific value from the preferences
	 *
	 * @param context Context application
	 * @return Boolean to indicate success or fail operation
	 */
	public static boolean remove(Context context, String key){
		return getSharedPreferences(context).edit().remove(key).commit();
	}

}
