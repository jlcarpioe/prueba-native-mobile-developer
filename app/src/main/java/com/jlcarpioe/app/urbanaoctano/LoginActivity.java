package com.jlcarpioe.app.urbanaoctano;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.jlcarpioe.app.urbanaoctano.enums.API_ROUTE;
import com.jlcarpioe.app.urbanaoctano.enums.PARAM_TYPE;
import com.jlcarpioe.app.urbanaoctano.interfaces.ApiErrorListener;
import com.jlcarpioe.app.urbanaoctano.models.ApiError;
import com.jlcarpioe.app.urbanaoctano.models.ApiParam;
import com.jlcarpioe.app.urbanaoctano.models.ResponseLogin;
import com.jlcarpioe.app.urbanaoctano.net.RequestsManager;
import com.jlcarpioe.app.urbanaoctano.utils.SharedPreference;
import com.jlcarpioe.app.urbanaoctano.utils.Util;
import com.jlcarpioe.app.urbanaoctano.utils.Validator;
import com.jlcarpioe.app.urbanaoctano.widgets.ProgressLoader;

import java.util.ArrayList;


/**
 * LoginActivity.
 * A login screen that offers login via rut/password.
 *
 * This code is under the MIT License (MIT). See LICENSE file.
 *
 * @author José Luis Carpio E. <jlcarpioe@gmail.com>.
 *
 */
public class LoginActivity extends AppCompatActivity {

	private static final String TAG = LoginActivity.class.getSimpleName();

    // UI references.
	private View mContainer;
	private TextInputLayout mRutContainer;
	private TextInputLayout mPasswordContainer;
    private TextInputEditText mRutInput;
    private TextInputEditText mPasswordInput;

    private ProgressLoader mProgressLoader;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
		findViewElements();

		// Instance progress loader
	    mProgressLoader = ProgressLoader.setup(this);

        Button mSignInButton = findViewById(R.id.login_button);
        mSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

    }


	/**
	 * Find elements on layout
	 *
	 */
	private void findViewElements() {
		mContainer = findViewById(R.id.rut_login_form);
		mRutContainer= findViewById(R.id.login_rut_container);
		mPasswordContainer = findViewById(R.id.login_password_container);
		mRutInput = findViewById(R.id.login_rut);
		mPasswordInput = findViewById(R.id.login_password);
	}


	/**
	 * Attempts to sign in by the login form.
	 * If there are form errors (invalid password, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	private void attemptLogin() {
		Util.hideKeyboard(this);

		// Reset errors.
		mRutContainer.setError(null);
		mRutContainer.setErrorEnabled(false);

		mPasswordContainer.setError(null);
		mPasswordContainer.setErrorEnabled(false);

		// Store values at the time of the login attempt.
		String rut = mRutInput.getText().toString();
		String password = mPasswordInput.getText().toString();

		boolean cancel = false;
		View focusView = null;


		// Check for a empty password.
		if (Validator.isEmpty(mPasswordInput)) {
			mPasswordContainer.setError(getString(R.string.error_field_required));
			mPasswordContainer.setErrorEnabled(true);
			focusView = mPasswordInput;
			cancel = true;
		}
		// Check for a valid password, if the user entered one.
		else if (!Validator.isValidPassword(mPasswordInput)) {
			mPasswordContainer.setError(getString(R.string.error_invalid_password));
			mPasswordContainer.setErrorEnabled(true);
			focusView = mPasswordInput;
			cancel = true;
		}

		// Check for a empty rut.
		if (Validator.isEmpty(mRutInput)) {
			mRutContainer.setError(getString(R.string.error_field_required));
			mRutContainer.setErrorEnabled(true);
			focusView = mRutInput;
			cancel = true;
		}
		// Check for a valid rut.
		else if (!Validator.isValidRut(mRutInput)) {
			mRutContainer.setError(getString(R.string.error_invalid_rut));
			mRutContainer.setErrorEnabled(true);
			focusView = mRutInput;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {

			// Show loader while request is execute
			mProgressLoader.show();

			// Params request
			ArrayList<ApiParam> requestParams = new ArrayList<>();
			requestParams.add(new ApiParam("rut", PARAM_TYPE.BODY, rut));
			requestParams.add(new ApiParam("password", PARAM_TYPE.BODY, password));

			// Service request
			RequestsManager.request(
					this,
					requestParams,
					API_ROUTE.LOGIN,
					ResponseLogin.class,
					ApiError.class,
					new Response.Listener<ResponseLogin>() {
						@Override
						public void onResponse(ResponseLogin response) {

							// save token
							SharedPreference.save(LoginActivity.this, "token_header", response.getToken());

							// mark user as logged
							SharedPreference.save(LoginActivity.this, "logged", true);

							// hide progress
							mProgressLoader.dismiss();

							// Change activity
							startActivity(new Intent(LoginActivity.this, MainActivity.class));

							finish();
						}
					},
					new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
							Log.e(TAG, "VolleyError :: " + error.getMessage());

							mProgressLoader.dismiss();

							// Alert message
							new AlertDialog.Builder(LoginActivity.this)
									.setMessage(R.string.request_error)
									.setPositiveButton(R.string.go_ahead, new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											dialog.dismiss();
										}
									})
									.create()
									.show();

						}
					},
					new ApiErrorListener<ApiError>() {
						@Override
						public void onApiError(ApiError apiError) {
							Log.e(TAG, "ApiError :: (" + apiError.getErrorCode() + ") " + apiError.getErrorMessage());

							mProgressLoader.dismiss();

							// Snackbar info
							Snackbar snackbar = Snackbar.make(mContainer, R.string.error_login, Snackbar.LENGTH_LONG);
							View snackbarView = snackbar.getView();
							snackbarView.setBackgroundColor(ContextCompat.getColor(LoginActivity.this, R.color.red));
							snackbar.show();
						}
					}
			);

		}
	}


}

