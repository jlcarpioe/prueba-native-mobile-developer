package com.jlcarpioe.app.urbanaoctano.utils;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * Util
 * General operations utilities.
 *
 * This code is under the MIT License (MIT). See LICENSE file.
 *
 * @author José Luis Carpio E. <jlcarpioe@gmail.com>.
 *
 */
public class Util {

	private static final String TAG = Util.class.getSimpleName();


	/**
	 * To hide the keyword.
	 *
	 * @param context Context application
	 */
	public static void hideKeyboard(Context context) {
		View view = ((Activity)context).getCurrentFocus();
		if (view != null) {
			InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
			inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}


	/**
	 * String to MD5.
	 *
	 * @param text String to convert
	 * @return MD5 String
	 */
	public static String MD5(String text) {

		if (text == null) {
			return null;
		}

		try {
			StringBuilder sb = new StringBuilder();

			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] array = md.digest(text.getBytes());
			for (byte anArray : array) {
				sb.append(Integer.toHexString((anArray & 0xFF) | 0x100).substring(1, 3));
			}

			return sb.toString();
		}
		catch (NoSuchAlgorithmException e) {
			Log.e(TAG, "MD5 :: NoSuchAlgorithmException\n", e);
		}

		return null;
	}

}
