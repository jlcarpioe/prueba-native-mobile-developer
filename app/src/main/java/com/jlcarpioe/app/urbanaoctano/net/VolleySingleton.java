package com.jlcarpioe.app.urbanaoctano.net;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;


/**
 * VolleySingleton.
 * Implements a singleton class that encapsulates RequestQueue and other Volley functionality.
 *
 * This code is unde the MIT License (MIT). See LICENSE file.
 *
 * @author José Luis Carpio E. <jlcarpioe@gmail.com>
 *
 * @see com.android.volley.RequestQueue Single Instance <https://developer.android.com/training/volley/requestqueue.html#singleton>
 *
 */
public class VolleySingleton {

	private static VolleySingleton mInstance;
	private RequestQueue mRequestQueue;
	private ImageLoader mImageLoader;
	private static Context mContext;


	/**
	 * Contructor for class.
	 *
	 * @param context Context application.
	 *
	 */
	private VolleySingleton(Context context) {
		mContext = context;
		mRequestQueue = getRequestQueue();

		mImageLoader = new ImageLoader(mRequestQueue,
			new ImageLoader.ImageCache() {
				private final LruCache<String, Bitmap> cache = new LruCache<>(20);

				@Override
				public Bitmap getBitmap(String url) {
					return cache.get(url);
				}

				@Override
				public void putBitmap(String url, Bitmap bitmap) {
					cache.put(url, bitmap);
				}
			});
	}

	/**
	 * Instance of singleton
	 *
	 * @param context Context application.
	 * @return Instance of VolleySingleton.
	 *
	 */
	public static synchronized VolleySingleton getInstance(Context context) {
		if (mInstance == null) {
			mInstance = new VolleySingleton(context);
		}
		return mInstance;
	}

	/**
	 * Get request queue.
	 *
	 * @return RequestQueue
	 *
	 */
	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			// getApplicationContext() is key, it keeps you from leaking the
			// Activity or BroadcastReceiver if someone passes one in.
			mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
		}
		return mRequestQueue;
	}

	/**
	 * Add request to queue.
	 *
	 * @param request {@link GsonRequest}
	 * @param <T> Sucess Class for JsonObject
	 * @param <S> Error Class for JsonObject
	 */
	public <T,S> void addToRequestQueue(GsonRequest<T,S> request) {
		getRequestQueue().add(request);
	}

	/**
	 * Add request json to queue.
	 *
	 * @param request {@link GsonRequestAPI}
	 * @param <T> Sucess Class for JsonObject
	 * @param <S> Error Class for JsonObject
	 */
	public <T,S> void addToRequestQueue(GsonRequestAPI<T,S> request) {
		getRequestQueue().add(request);
	}

	/**
	 * Returns {@link ImageLoader} volley object.
	 *
	 * @return ImageLoader instance.
	 *
	 */
	public ImageLoader getImageLoader() {
		return mImageLoader;
	}


}
