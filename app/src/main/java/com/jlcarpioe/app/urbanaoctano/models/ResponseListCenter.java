package com.jlcarpioe.app.urbanaoctano.models;

import com.google.gson.annotations.Expose;

import java.util.List;


/**
 * ResponseListCenter.
 * Handle response data when request list center.
 *
 * @author José Luis Carpio E. <jlcarpioe@gmail.com>.
 *
 */
public class ResponseListCenter {

	@Expose
	private List<Center> data;


	public List<Center> getCenters() {
		return data;
	}

}
