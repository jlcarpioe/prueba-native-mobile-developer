package com.jlcarpioe.app.urbanaoctano.utils;

import android.text.TextUtils;
import android.widget.EditText;


/**
 * Validator.
 * General validations for entry values.
 *
 * This code is under the MIT License (MIT). See LICENSE file.
 *
 * @author José Luis Carpio E. <jlcarpioe@gmail.com>.
 *
 */
public class Validator {

	/**
	 * Validate input if it is empty.
	 *
	 * @param editText Input
	 * @return boolean Indicate if it is empty or not
	 */
	public static boolean isEmpty(EditText editText) {
		return TextUtils.isEmpty(editText.getText().toString());
	}

	/**
	 * Validate input for rut.
	 *
	 * @param editText Rut input
	 * @return boolean Indicate if it is valid or not
	 */
	public static boolean isValidRut(EditText editText) {
		return !TextUtils.isEmpty(editText.getText().toString())
				&& editText.getText().toString().length() > 7;
	}

	/**
	 * Validate input for password.
	 *
	 * @param editText Password input
	 * @return boolean Indicate if it is valid or not
	 */
	public static boolean isValidPassword(EditText editText) {
		return !TextUtils.isEmpty(editText.getText().toString())
				&& editText.getText().toString().length() > 7;
	}

}
