package com.jlcarpioe.app.urbanaoctano.interfaces;


import com.jlcarpioe.app.urbanaoctano.models.Center;

/**
 * OnListCenterListener
 * Actions listener for centers selection.
 *
 * @author José Luis Carpio E. <jlcarpioe@gmail.com>
 *
 */
public interface OnListCenterListener {

	void onCenterSelected(Center center);

}
