package com.jlcarpioe.app.urbanaoctano.models;

import com.google.gson.annotations.Expose;

/**
 * ResponseLogin.
 * Handle response data when it has a success sign in.
 *
 * @author José Luis Carpio E. <jlcarpioe@gmail.com>.
 *
 */
public class ResponseLogin {

	@Expose
	private String token;

	public String getToken() {
		return token;
	}
}
